FROM nginx:1.17-alpine
EXPOSE 80
WORKDIR /usr/share/nginx/html/assets/json/

RUN rm -rf /usr/share/nginx/html/*

ADD config/nginx.conf /etc/nginx/nginx.conf
COPY dist /usr/share/nginx/html/

CMD ["/bin/sh", "-c", "exec nginx -g 'daemon off;'" ]