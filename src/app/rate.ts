export interface Rate {
  zeitraumStart: Date;
  zeitraumEnde: Date;
  restschuld: number;

  zahlung: number;
  zahlungSumme: number;
  zinsen: number;
  zinsenSumme: number;
  tilgung: number;
  tilgungSumme: number;

  sondertilgung: number;
  sondertilgungSumme: number;

  restschuldNeu: number;

  details?: Rate[];
  showDetails: boolean;
  isEndeZinsbindung: boolean;
}
