import { Sondertilgung } from './../sondertilgung';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {
  map,
  startWith,
  debounceTime,
  filter,
  groupBy,
  switchMap,
  mergeMap,
  toArray,
  tap,
} from 'rxjs/operators';
import {
  Observable,
  combineLatest,
  from,
  GroupedObservable,
  of,
  ReplaySubject,
} from 'rxjs';
import { Rate } from '../rate';

const DEBOUNCE_TIME = 300;
const JAHRE_IN_MONATE_5 = 60;

@Component({
  selector: 'app-tilgung-details',
  templateUrl: './tilgung-details.component.html',
  styleUrls: ['./tilgung-details.component.css'],
})
export class TilgungDetailsComponent implements OnInit {
  zinsbindungOptionen = [5, 10, 15, 20, 25, 30];

  kredithoeheControl = new FormControl(375000, Validators.min(0));
  zinssatzControl = new FormControl(1.21, Validators.min(0));
  tilgungssatzControl = new FormControl(2.0, Validators.min(0.1));
  kreditbeginnControl = new FormControl();
  zeigeSummenSpaltenControl = new FormControl();
  zinsbindungControl = new FormControl();

  sondertilgungDateControl = new FormControl();
  sondertilgungHoeheControl = new FormControl();

  monatsrate$: Observable<number>;
  kredithoehe$: Observable<any>;
  zinssatz$: Observable<number>;
  tilgungssatz$: Observable<number>;
  zinsbindung$: Observable<number>;

  ratenplan$: Observable<Rate[][]>;

  sondertilgungen$ = new ReplaySubject<Sondertilgung[]>(1);
  kreditbeginn$: Observable<Date>;

  refreshTable = new ReplaySubject<void>(1);

  sondertilgungen: Sondertilgung[] = [];
  summeSondertilgung$: Observable<number>;

  kummulierteSpaltenAnzeigen;

  constructor() {}

  ngOnInit() {
    this.kredithoehe$ = this.kredithoeheControl.valueChanges.pipe(
      startWith(this.kredithoeheControl.value),
      debounceTime(DEBOUNCE_TIME)
    );
    this.zinssatz$ = this.zinssatzControl.valueChanges.pipe(
      startWith(this.zinssatzControl.value),
      debounceTime(DEBOUNCE_TIME),
      map((p) => p / 100)
    );
    this.tilgungssatz$ = this.tilgungssatzControl.valueChanges.pipe(
      startWith(this.tilgungssatzControl.value),
      debounceTime(DEBOUNCE_TIME),
      map((p) => p / 100)
    );
    this.kreditbeginn$ = this.kreditbeginnControl.valueChanges.pipe(
      startWith(new Date()),
      debounceTime(DEBOUNCE_TIME)
    );
    this.zinsbindung$ = this.zinsbindungControl.valueChanges.pipe(
      startWith(JAHRE_IN_MONATE_5),
      debounceTime(DEBOUNCE_TIME)
    );

    this.monatsrate$ = combineLatest([
      this.kredithoehe$,
      this.zinssatz$,
      this.tilgungssatz$,
    ]).pipe(
      filter(([kredithoehe, zinssatz, tilgungssatz]) => tilgungssatz > 0.0001),
      map(
        ([kredithoehe, zinssatz, tilgungssatz]) =>
          (kredithoehe * (zinssatz + tilgungssatz)) / 12
      ),
      map((monatsrate) => this.round(monatsrate, 2))
    );

    this.summeSondertilgung$ = this.sondertilgungen$.pipe(
      startWith(this.sondertilgungen),
      map((sondertilgungen) =>
        sondertilgungen
          .map((sondertilgung) => sondertilgung.sondertilgung)
          .reduce((p, c) => p + c, 0)
      )
    );

    //
    const jahresratenplan$ = combineLatest([
      this.kredithoehe$,
      this.monatsrate$,
      this.zinssatz$,
      this.zinsbindung$,
      this.tilgungssatz$,
      this.kreditbeginn$,
      this.sondertilgungen$
        .asObservable()
        .pipe(startWith(this.sondertilgungen)),
    ]).pipe(
      filter(
        ([
          kredithoehe,
          monatsrate,
          zinssatz,
          zinsbindung,
          tilgungssatz,
          kreditbeginn,
          sondertilgungen,
        ]) => kredithoehe > 100 && monatsrate > 1 && zinssatz > 0.0001
      ),
      map(
        ([
          kredithoehe,
          monatsrate,
          zinssatz,
          zinsbindung,
          tilgungssatz,
          kreditbeginn,
          sondertilgungen,
        ]) => {
          return this.kalkuliereRatenplan(
            kredithoehe,
            monatsrate,
            zinssatz,
            zinsbindung,
            sondertilgungen,
            kreditbeginn
          );
        }
      ),
      switchMap((monatsplan) =>
        from(monatsplan).pipe(
          groupBy((monatsrate) => monatsrate.zeitraumEnde.getFullYear()),
          mergeMap((group) => group.pipe(toArray())),
          map((monatsraten) =>
            this.reduceToOne(monatsraten, monatsraten.length < 12)
          ),
          toArray()
        )
      )
    );

    this.ratenplan$ = combineLatest([
      jahresratenplan$,
      this.refreshTable.pipe(startWith(null as void)),
    ]).pipe(map(([jahresratenplan]) => jahresratenplan));

    //
  }

  public showSummen(): void {
    // TODO Checkbox value auswerten (zeigeSummenSpaltenControl)
    this.kummulierteSpaltenAnzeigen = !this.kummulierteSpaltenAnzeigen;
    this.refreshTable.next(null as void);
  }

  public addSondertilgung(): void {
    if (
      !this.sondertilgungDateControl.value ||
      !this.sondertilgungHoeheControl.value
    ) {
      return;
    }

    const sondertilgung = {
      tag: this.addMonths(this.sondertilgungDateControl.value, 0),
      sondertilgung: this.sondertilgungHoeheControl.value,
    };
    this.sondertilgungen.push(sondertilgung);
    this.sondertilgungen$.next(this.sondertilgungen);
  }

  public deleteSondertilgung(
    sondertilgung: Sondertilgung,
    index: number
  ): void {
    this.sondertilgungen.splice(index, 1);
    this.sondertilgungen$.next(this.sondertilgungen);
  }

  public toggleShowDetails(jahresrate: Rate): void {
    jahresrate.showDetails = !jahresrate.showDetails;
    console.log(jahresrate.showDetails);
    this.refreshTable.next(null as void);
  }

  private kalkuliereRatenplan(
    kredithoehe: any,
    monatsrate: number,
    zinssatz: number,
    zinsbindung: any,
    sondertilgungen: Sondertilgung[],
    startMonat: Date
  ): Rate[] {
    const ratenplanDaten = new Array<Rate>();

    let restschuld = kredithoehe;
    let zahlungSumme = 0;
    let zinsenSumme = 0;
    let tilgungSumme = 0;
    let sondertilgungSumme = 0;

    let monat = 0;

    while (true) {
      if (restschuld <= 0) {
        break;
      }

      const zeitraumStart = this.addMonths(startMonat, monat, false);
      const zeitraumEnde = this.addMonths(startMonat, monat, true);
      const zinsen = this.round(restschuld * (zinssatz / 12), 2);
      const tilgung = Math.min(Math.max(monatsrate - zinsen, 0), restschuld);
      const sondertilgung = sondertilgungen
        .filter((s) => s.tag.getTime() === zeitraumEnde.getTime())
        .map((s) => s.sondertilgung)
        .reduce((p, c) => p + c, 0);
      const restschuldNeu = this.round(restschuld - tilgung - sondertilgung, 2);
      const zahlung = zinsen + tilgung;

      zahlungSumme += zahlung;
      zinsenSumme += zinsen;
      tilgungSumme += tilgung;
      sondertilgungSumme += sondertilgung;

      const data = {
        zeitraumStart,
        zeitraumEnde,
        restschuld,
        zahlung,
        zahlungSumme,
        zinsen,
        zinsenSumme,
        tilgung,
        tilgungSumme,
        sondertilgung,
        sondertilgungSumme,
        restschuldNeu,
        showDetails: false,
        isEndeZinsbindung: zinsbindung === monat + 1,
      } as Rate;

      restschuld = restschuldNeu;

      ratenplanDaten.push(data);
      monat++;
    }

    return ratenplanDaten;
  }

  private round(value: number, digits: number): number {
    const m = Math.pow(10, digits);
    return Math.round(value * m) / m;
  }

  private reduceToOne(monatsraten: Rate[], showDetails: boolean): Rate[] {
    const jahresrate = monatsraten.reduce((prev, curr) => {
      const isEndeZinsbindung =
        prev.isEndeZinsbindung || curr.isEndeZinsbindung;

      return {
        zeitraumStart: prev.zeitraumStart,
        zeitraumEnde: curr.zeitraumEnde,
        restschuld: prev.restschuld,
        restschuldNeu: curr.restschuldNeu,
        zahlung: prev.zahlung + curr.zahlung,
        zahlungSumme: curr.zahlungSumme,
        zinsen: prev.zinsen + curr.zinsen,
        zinsenSumme: curr.zinsenSumme,
        tilgung: prev.tilgung + curr.tilgung,
        tilgungSumme: curr.tilgungSumme,
        sondertilgung: prev.sondertilgung + curr.sondertilgung,
        sondertilgungSumme: curr.sondertilgungSumme,
        showDetails: showDetails || isEndeZinsbindung,
        isEndeZinsbindung,
      } as Rate;
    });

    jahresrate.details = monatsraten;

    return [jahresrate];
  }

  private addMonths(date: Date, count: number, lastOfMonth = true) {
    const newDate = new Date(date.getTime());
    newDate.setDate(1);
    newDate.setMonth(newDate.getMonth() + count + (lastOfMonth ? 1 : 0));
    newDate.setDate(lastOfMonth ? newDate.getDate() - 1 : 1);
    newDate.setHours(0);
    newDate.setMinutes(0);
    newDate.setSeconds(0);
    newDate.setMilliseconds(0);

    return newDate;
  }
}
